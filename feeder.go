package main

import (
  // "encoding/json"
  "fmt"
  // "io/ioutil"
  "net/http"
  "github.com/jasonlvhit/gocron"
  "math/rand"
  "time"
  "strings"
  "io/ioutil"
  "strconv"
)

type Stream struct {
      Id          int       `json:"id"         validate:"required`
      ID1         int       `json:"ID1"        validate:"required`
      ID2         string    `json:"ID2"        validate:"required"`
      Value       int       `json:"v"          validate:"required"`
      Timestamp   int       `json:"t"          validate:"required"`
}


func task() {
  url := "http://127.0.0.1:4001/stream/add"
  method := "POST"

  var ID1 = strconv.Itoa(rand.Intn(9 - 1) + 1)
  var ID2 = "A"
  var v = strconv.Itoa(rand.Intn(100 - 1) + 1)
  var t = strconv.Itoa(int(time.Now().Unix()))

 rand.Seed(time.Now().Unix())

  //Lowercase and Uppercase Both
  var output strings.Builder
  var charSet = "ABCD"
  var length = 1
  for i := 0; i < length; i++ {
      random := rand.Intn(len(charSet))
      randomChar := charSet[random]
      output.WriteString(string(randomChar))
  }
  fmt.Println(output.String())

  ID2 = output.String()

  payload := strings.NewReader(`{
    "ID1" : `+ ID1 +`,
    "ID2" : "`+ ID2 +`",
    "v" : `+ v +`,
    "t" : `+ t +`
}`)

fmt.Println(payload)

  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, payload)

  if err != nil {
    fmt.Println(err)
    return
  }
  req.Header.Add("Content-Type", "application/json")

  res, err := client.Do(req)
  if err != nil {
    fmt.Println(err)
    return
  }
  defer res.Body.Close()

  body, err := ioutil.ReadAll(res.Body)
  if err != nil {
    fmt.Println(err)
    return
  }
  fmt.Println(string(body))
}


func main() {
  s := gocron.NewScheduler()
  s.Every(1).Seconds().Do(task)
  <- s.Start()
}
