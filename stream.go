package main

import (
  _ "github.com/go-sql-driver/mysql"
  "github.com/labstack/echo"
  "github.com/go-playground/validator/v10"
  "github.com/labstack/echo/middleware"
  "database/sql"
  "fmt"
  "time"
  "net/http"
  "strconv"
)

// Run Before Creating File
// $ go get github.com/labstack/echo/v4
// $ go get github.com/labstack/echo/middleware
// $ go get github.com/go-sql-driver/mysql

type Stream struct {
      Id          int       `json:"id"         validate:"required`
      ID1         int       `json:"ID1"        validate:"required`
      ID2         string    `json:"ID2"        validate:"required"`
      Value       int       `json:"v"          validate:"required"`
      Timestamp   int       `json:"t"          validate:"required"`
}

type List struct {
      Value       int       `json:"Value"          validate:"required"`
      ID1         int       `json:"ID1"            validate:"required`
      ID2         string    `json:"ID2"            validate:"required"`
      Timestamp   string       `json:"Timestamp"      validate:"required"`
}

type CustomValidator struct {
    validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
    return cv.validator.Struct(i)
}

func main() {
  e := echo.New()

  e.Validator = &CustomValidator{validator: validator.New()}

  // Debug mode
  e.Debug = true

  e.Use(middleware.Logger())
  e.Use(middleware.Recover())

  e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
  			AllowOrigins: []string{"*"},
  			AllowMethods: []string{echo.GET, echo.POST},
  }))

  // CREATE TABLE IF NOT EXISTS `stream` (
  //   `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  //   `ID1` int(8) NOT NULL COMMENT 'ID1',
  //   `ID2` varchar(255) NOT NULL COMMENT 'ID2',
  //   `value` int(8) NOT NULL COMMENT 'Stream Value',
  //   `timestamp` int(12) NOT NULL COMMENT 'Timestamp',
  //   PRIMARY KEY (`id`)
  // ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='datatable demo table' AUTO_INCREMENT=0 ;


  db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
	if err != nil {
				fmt.Println(err.Error())
	} else {
				fmt.Println("db is connected")
	}
	defer db.Close()
	// make sure connection is available
	err = db.Ping()
	if err != nil {
				fmt.Println(err.Error())
	}

  e.POST("/stream/add", func(c echo.Context) error {
    stream := new(Stream)

    if err := c.Bind(stream); err != nil {
          return err
    }

    if err := c.Validate(stream); err != nil {
        return err
    }

    //
    sql := "INSERT INTO stream(ID1, ID2, value, timestamp) VALUES( ?, ?, ?, ?)"
    stmt, err := db.Prepare(sql)

    if err != nil {
          fmt.Print(err.Error())
    }
    defer stmt.Close()

    fmt.Println("timestamp:", time.Now().Unix())
    result, err2 := stmt.Exec(stream.ID1, stream.ID2, stream.Value, stream.Timestamp)

    // Exit if we get an error
    if err2 != nil {
          return err2
    }

    id, err := result.LastInsertId()
    if err == nil {
      stream.Id = int(id)
    }

    fmt.Println(stream)

    return c.JSON(http.StatusCreated, stream)
  })

  e.GET("/stream/data", func(c echo.Context) error {
    ID1 := c.QueryParam("ID1")
    ID2 := c.QueryParam("ID2")
    start := c.QueryParam("start_timestamp")
    end := c.QueryParam("end_timestamp")

    var query = "SELECT ID1, ID2, value, timestamp FROM stream WHERE 1=1 "

    if ID1 != "" {
      query = query + " AND ID1=" + ID1
    }

    if ID2 != "" {
      query = query + " AND ID2='" + ID2 + "'"
    }

    if start != "" {
      query = query + " AND timestamp >= " + start
    }

    if end != "" {
      query = query + " AND timestamp < " + end
    }


    query = query + " LIMIT 20"

    fmt.Println(query)

    rows, err := db.Query(query)

    if err != nil {
      return err
    }

    defer rows.Close()

    var streams []List

    // Loop through rows, using Scan to assign column data to struct fields.
    for rows.Next() {
      var stream List
       if err := rows.Scan(&stream.ID1, &stream.ID2, &stream.Value, &stream.Timestamp); err != nil {
           return err
       }

      t, err := strconv.ParseInt(stream.Timestamp, 10, 64)
      if err != nil {
        return err
      }

      stream.Timestamp = time.Unix(t, 0).Format("Mon, 2 Jan 2006 15:04:05 MST")
      fmt.Println(stream.Timestamp)
      streams = append(streams, stream)
    }

    if err = rows.Err(); err != nil {
      return err
    }

    return c.JSON(http.StatusOK,streams)
  })

  e.Logger.Fatal(e.Start(":4001"))
}
